const jwt =require("jsonwebtoken"); 



const secret="4bbe80dc-34d9-452b-b4ee-1616882d88ce";

module.exports.createAccessToken=(user)=>{
	const data={
		id:user._id,
		email:user.email,
		userType:user.userType,
		isAdmin:user.isAdmin


	}
	return jwt.sign(data,secret,{})
}


//middleware functions
module.exports.verify=(req,res,next)=>{
	let token=req.headers.authorization;
	if(typeof token !=="undefined"){
			token=token.slice(7,token.length);
			return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return res.send({auth:"Token Failed"});
			}else{
				next();
			}

			})

	}else{
		return res.send({auth:"Failed"});
	}
}

module.exports.decode=(token)=>{

	if(typeof token !=="undefined"){
			token=token.slice(7,token.length);
			return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return null;
			}else{
				return jwt.decode(token,{complete:true}).payload;
			}

			})

	}else{
		null;
	}

}
