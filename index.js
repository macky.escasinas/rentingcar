const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const carRoutes = require("./routes/carRoutes");
const parameterRoutes = require("./routes/parameterRoutes");
const applicationRoutes = require("./routes/applicationRoutes");
const parkingRoutes = require("./routes/parkingRoutes");
const apiRoutes = require("./routes/apiRoutes");
const transactionRoutes = require("./routes/transactionRoutes");
const app = express();
const port = 3001;


app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://rentingCarsAdmin:n4efKK8zgozFr8Bj@rentingcars.zo3acre.mongodb.net/");


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/cars",carRoutes);

app.use("/users",userRoutes);
app.use("/parameters",parameterRoutes);
app.use("/application",applicationRoutes);
app.use("/parking",parkingRoutes);
app.use("/api",apiRoutes);
app.use("/transaction",transactionRoutes);
app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${port}`));