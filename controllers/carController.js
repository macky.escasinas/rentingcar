const Application=require("../models/Application");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const User=require("../models/User");
const Car=require("../models/Car");
const History=require("../models/TransactionHistory");
const transporter = nodemailer.createTransport(smtpTransport({
				port:465,
				secure:true,
				name: 'mail.thefavis.com',
		     host: 'mail.thefavis.com',
		     auth: {
		         user: 'info@thefavis.com',
		         pass: 'TheFavis2023',
		     },

		     tls: {
		            rejectUnauthorized: false
		        },
					logger: false,
					debug: false,
		 			sendmail: true 

		}));


module.exports.viewAllActiveCar=()=>{
	return Car.find({ userId:"host",isActive:true}).then(result=>result);
}

module.exports.viewCurrentCar=(vehicleId,userId)=>{
  return Car.findOne({ _id:vehicleId,isActive:true,userId:userId}).then(result=>result);
}


module.exports.viewAllUserCar=()=>{
	return Car.find({type:"CO-HOST",isActive:true}).then(result=>result);
}
module.exports.viewAllByApplication=(applicationId)=>{
	return Car.find({applicationRef:applicationId,isActive:true}).then(result=>result);
}
module.exports.viewAllCoHostCar=(userId)=>{
	return Car.find({userId:userId,isActive:true}).then(result=>result);
}
module.exports.viewAllNoApplication=(userId)=>{
  return Car.find({userId:userId,isActive:true,applicationRef:""}).then(result=>result);
}
module.exports.viewAllAffiliateCar=()=>{
	return Car.find( { userId: { $ne: "host" } ,isActive:true}).then(result=>result);
}





module.exports.createCar=(reqBody)=>{


let newCar= new Car({
		userId: "host",
		make: "", 
		brand: "",
		description: "",
		price: 0,
		carImage: "",
    type:"",
		location: "",
    turoLink:"turoLink",
    gas:"",
    transmission:"",
    trips:"trips"
	})

	return 	newCar.save().then((car,error)=>{
		if(error){			
			return false;
		}else{	
			return true;
		}
	});
}

module.exports.deleteCar= async(reqParam,userId)=>{

			let carToDelete= await Car.findById(reqParam).then(result=>result);
			let UserToInfo= await User.findById(userId).then(result=>result);

	return 	Car.updateOne({_id:reqParam}, {"$set":{"isActive": false}}).then((car,error)=>{

		if(error){			
			return false;
		}else{	
			sendCustomerCarNotification(carToDelete,UserToInfo);
			
			return true;
		}
	});
}
module.exports.updateSubscription= (reqBody,userId)=>{

  return  Car.findByIdAndUpdate(reqBody.carId, {"$set":{"services":reqBody.services}}).then((car,error)=>{

    if(error){      
      return false;
    }else{  
      
      
      return true;
    }
  });
}
module.exports.additionalServices= (reqBody,userId)=>{

  return  Car.findByIdAndUpdate(reqBody.carId, {"$set":{"additionalServices":reqBody.additionalServices}}).then((car,error)=>{

    if(error){      
      return false;
    }else{  
      
      
      return true;
    }
  });
}

module.exports.addCarToApplication=(userId,reqBody)=>{

let newCar= new Car({
		userId: userId,
		make: reqBody.make,
		year: reqBody.year,
		model: reqBody.model,
		condition: reqBody.condition,
		costRepair: reqBody.costRepair,
		marketValue: reqBody.marketValue,
		type: reqBody.type,
		carImage:reqBody.carImage,
		services:reqBody.services,
		additionalServices:reqBody.additionalServices,
		message:reqBody.message
		
	})


	return 	newCar.save().then((car,error)=>{
		if(error){			
			return false;
		}else{	
			return true;
		}
	});
}



const sendCustomerCarNotification=(carInfo,userInfo)=>{

	 const {make}=carInfo;
	  const {email,firstName,lastName,phone}=userInfo;
    


  const subject="The Favis Application Update";
  const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">Inquiries Confirm Welcome to The Favis Car Rental Vehicle Rental Management Services! we will get back to you as soon as possible.<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#ffffff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:40.5em;margin:0 auto;padding:20px 0 48px">
      <tr style="width:100%">
        <td><img alt="TheFavis" src="https://the-favis-p5cw.vercel.app/main-logo.png"  style="display:block;outline:none;border:none;text-decoration:none;margin:0 auto" />
          <p style="font-size:16px;line-height:26px;margin:16px 0">Dear ${firstName} ${lastName},</p>
          <p style="font-size:16px;line-height:26px;margin:0px 0">
This is to confirm that you have removed the ${make} from your co-hosting application. Please note that once your application has been approved, you will not be able to amend the application details.
      <br/><a href="https://the-favis-p5cw.vercel.app/login">Go to the admin portal</a>
          <table style="text-align:center" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            
          </table>
          <p style="font-size:16px;line-height:26px;margin:16px 0">Best Regards,<br />Sales Department The Favis<br/> (281-825-1514/sales@thefavis.com)
          </p>
           <p style="font-size:12px;line-height:24px;margin:16px 0;color:#8898aa">2315 Greens Road, Houston TX, 77032</p>
         
          <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />
                  <table align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <table width="100%" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                            <tbody style="width:100%">
                              <tr style="width:100%">
                                <a href="https://thefavis.com/" target="_blank" style="background-color:#168b74;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;line-height:100%;max-width:100%;padding:10px 10px"><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%;mso-text-raise:15" hidden>&nbsp;</i><![endif]--></span><span style="background-color:#168b74;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;max-width:100%;line-height:120%;text-transform:none;mso-padding-alt:0px;mso-text-raise:7.5px">Go to our Website</span><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a>
                                <hr style="width:100%;border:none;border-top:1px solid #168b74;border-color:#168b74;margin:20px 0" />

         
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>

        </td>
      </tr>
    </table>

  </body>

</html>`;
  const mailOptions = {
         from: 'The Favis <info@thefavis.com>',
        to: email,
        subject:subject,
        html: html,
    };

  return  transporter.sendMail(mailOptions).then((info,error)=>{
        if(error){
          return false;
        }else{
     	sendAdminCarNotification(carInfo,userInfo);
          return true;
        }
    })

	}


const sendAdminCarNotification=(carInfo,userInfo)=>{
	 const {make}=carInfo;
	  const {email,firstName,lastName,phone}=userInfo;
     
     var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = mm + '/' + dd + '/' + yyyy;


  const subject="The Favis Application";
  const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">Inquiries Confirm Welcome to The Favis Car Rental Vehicle Rental Management Services! we will get back to you as soon as possible.<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#ffffff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:40.5em;margin:0 auto;padding:20px 0 48px">
      <tr style="width:100%">
        <td><img alt="TheFavis" src="https://the-favis-p5cw.vercel.app/main-logo.png"  style="display:block;outline:none;border:none;text-decoration:none;margin:0 auto" />
          <p style="font-size:16px;line-height:26px;margin:16px 0">Dear Sales Team,</p>
          <p style="font-size:16px;line-height:26px;margin:0px 0">
We would like to inform you that changes have been made to remove ${make} for ${firstName} ${lastName} on ${today}. To review these changes, kindly log in to the admin portal or click on the provided link.
      <br/><a href="https://the-favis-p5cw.vercel.app/login">Go to the admin portal</a>
          <table style="text-align:center" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            
          </table>
          <p style="font-size:16px;line-height:26px;margin:16px 0">Best Regards,<br />Sales Department The Favis<br/> (281-825-1514/sales@thefavis.com)
          </p>
           <p style="font-size:12px;line-height:24px;margin:16px 0;color:#8898aa">2315 Greens Road, Houston TX, 77032</p>
         
          <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />
                  <table align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <table width="100%" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                            <tbody style="width:100%">
                              <tr style="width:100%">
                                <a href="https://thefavis.com/" target="_blank" style="background-color:#168b74;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;line-height:100%;max-width:100%;padding:10px 10px"><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%;mso-text-raise:15" hidden>&nbsp;</i><![endif]--></span><span style="background-color:#656ee8;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;max-width:100%;line-height:120%;text-transform:none;mso-padding-alt:0px;mso-text-raise:7.5px">Go to our Website</span><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a>
                                <hr style="width:100%;border:none;border-top:1px solid #168b74;border-color:#168b74;margin:20px 0" />

         
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>

        </td>
      </tr>
    </table>

  </body>

</html>`;
  const mailOptions = {
         from: 'The Favis <info@thefavis.com>',
        to: 'info@thefavis.com',
        subject:subject,
        html: html,
    };

  return  transporter.sendMail(mailOptions).then((info,error)=>{
        if(error){
          return false;
        }else{
         
          return true;
        }
    })

	}