
const TransactionHistory=require("../models/TransactionHistory");

module.exports.viewAllTransaction=()=>{
	return TransactionHistory.find({}).then(result=>result);
}

module.exports.viewClientTransaction=(userId)=>{
	return TransactionHistory.find({userId:userId}).then(result=>result
		);
}