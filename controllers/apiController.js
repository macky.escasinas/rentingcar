const auth=require("../auth");
const ApiKey=require("../models/ApiKey");


module.exports.checkApi= async(req)=>{
	let checkApi=await ApiKey.find({apiKey: req,isActive:true}).then(result => {
				if(result.length > 0){
				   return true;
			    }else{
				 return false;
			   }
	});

	return checkApi;
}
const genAPIKey = () => {
  
  return [...Array(30)]
    .map((e) => ((Math.random() * 36) | 0).toString(36))
    .join('');
};

module.exports.generateApi=()=>{

let newApiKey= new ApiKey({
		apiKey:genAPIKey(),
		
	})

	return 	newApiKey.save().then((api,error)=>{
		if(error){			
			return false;
		}else{	
			return api.apiKey;
		}
	});
}
module.exports.generateQr=async(plateNumber)=>{
var gen=await genAPIKey({plateNumber:plateNumber,date_time:date_time})
var date_time = new Date();
var crypto = require('crypto');
var qr=crypto.createHash('md5').update(gen).digest("hex");

	const promise1 = new Promise((resolve, reject) => {
  		resolve({"qr":qr});
	});
	return 	promise1;

}
const genQrKey = (plateNumber) => {
  
  return [...Array(30)]
    .map((e) => ((Math.random() * 36) | 0).toString(36))
    .join(plateNumber);
};