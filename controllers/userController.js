const User=require("../models/User");
const bcrypt=require("bcrypt");
const auth=require("../auth");
const applicationController=require("../controllers/applicationController");
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const TransactionHistory=require("../models/TransactionHistory");
const Parking=require("../models/Parking");
const Car=require("../models/Car");

const transporter = nodemailer.createTransport(smtpTransport({
				port:465,
				secure:true,
				name: 'mail.thefavis.com',
		     host: 'mail.thefavis.com',
		     auth: {
		         user: 'info@thefavis.com',
		         pass: 'TheFavis2023',
		     },

		     tls: {
		            rejectUnauthorized: false
		        },
					logger: false,
					debug: true,
		 			sendmail: true 
		}));

module.exports.viewAllUser=()=>{
	return User.find({}).then(result=>result);
}

module.exports.registerUser= async(reqBody)=>{
	let newUser= new User({
		email: reqBody.email,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		password: bcrypt.hashSync(reqBody.password,10),
		userType:reqBody.type,
		turoLink:reqBody.turoLink,
		address:reqBody.address,
		phone:reqBody.phone,
		vehicle:reqBody.vehicle,
		company:reqBody.company,
		city:reqBody.city,
		state:reqBody.state,
		zip:reqBody.zip,
		country:reqBody.country
		
	})
		let checkEmailExists=await User.find({email: reqBody.email}).then(result => {
			if(result.length > 0){
			   return true;
		    }else{
			 return false;
		   }
		});
		if(checkEmailExists){
			return "Email Already Register";
		}else{
			
			return 	newUser.save().then((user,error)=>{
					if(error){
						return false;
					}else{
						sendRegistration(reqBody,user._id);
						return true;
					}
				});
		}
}
module.exports.verifyUser=async(reqBody)=>{
	let userToupdate= await User.findById(reqBody.userId).then(result=>result);
	if(userToupdate.verificationStatus){
		let updateToVerify={
		verificationStatus:false
		}
	
		return User.findByIdAndUpdate(reqBody.userId,updateToVerify).then((updateToAdmin,error)=>{
		if(error){
		
			return true;
		}else{
		
			return false;
		}
	})
	

		
	}else{
		
			return false;
	}
	

}
module.exports.loginUser=(reqBody)=>{
	return 	User.findOne({email:reqBody.email}).then(result => {
		if(result==null){
			return false;
		}else{
		if(result.verificationStatus){
			return {
					status:true
				};

		}else{
			const isPasswordCorrect=bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access:auth.createAccessToken(result)}
			}else{		
				return {
					status:false
				};
			}

		}	
			
		}
	});
}
module.exports.addCredit = async(reqBody,userId) =>{

	let userToupdate= await User.findById(reqBody.userId).then(result=>result);
	let newAmount=userToupdate.credit+parseInt(reqBody.amount);
		let updateToVerify={
			credit:newAmount
		}
		
	
		return User.findByIdAndUpdate(reqBody.userId,updateToVerify).then((updateToAdmin,error)=>{
		if(error){
		
			return false;
		}else{
			addTransaction(reqBody,userId);
			return true;
		}
	})

}
function addTransaction(reqBody,userId){
		
	let newTransaction= new TransactionHistory({
		userId:reqBody.userId,
		refId:reqBody.userId,
		type:"Credit",
		description:reqBody.description,
		amount:reqBody.amount,
		transactBy:userId	
	})

	return 	newTransaction	.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}




module.exports.getProfile = (data) =>{
	
	return User.findById(data.userId).then(result =>{

		return result;
	})
}
module.exports.getFullProfile = (data) =>{
	return User.findById(data.userId).then(result =>{
		return {
			firstName:result.firstName,
			lastName:result.lastName,
			email:result.email,
			address:result.address,
			phone:result.phone,
			turoLink:result.turoLink
		};
	})
}

module.exports.viewAllCarOwner=()=>{
	return User.find({userType:"cohost"}).then(result=>result);
}
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	});
}



module.exports.changePassword=async(reqBody,userData)=>{

return 	User.findOne({email:userData.email}).then(result => {
		if(result==null){
			return false;
		}else{	
			const isPasswordCorrect=bcrypt.compareSync(reqBody[0],result.password);
			if(isPasswordCorrect){
				let updatePassword={
					password:bcrypt.hashSync(reqBody[1],10),
					isReset: false,
				}
				
					return User.findOneAndUpdate({email: userData.email},updatePassword).then((userToupdate,error)=>{
					if(error){
						return false;
					}else{		
						return true;
					}
				})
			}else{		
				return false;
			}
		}
	});
}


module.exports.resetPassword=async(reqBody)=>{
let userToupdate= await User.find({email: reqBody.email}).then(result=>result);
	let tempPass = (Math.random() + 1).toString(36).substring(7);

	let updatePassword={
		password:bcrypt.hashSync(tempPass,10),
		isReset: true,
	}
	if(userToupdate.password){
		return false;
	}else{
		return User.findOneAndUpdate({email: reqBody.email},updatePassword).then((userToupdate,error)=>{
			
		if(error){
			return false;
		}else{	
			sendForgotPassword(userToupdate,tempPass)
			return true;
		}
	})
	}
}
function sendForgotPassword(userToupdate,tempPass){
	const {email,firstName,lastName}=userToupdate;
	const subject="The Favis";
	const html=`<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/><!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		body {
			margin: 0;
			padding: 0;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		.desktop_hide,
		.desktop_hide table {
			mso-hide: all;
			display: none;
			max-height: 0px;
			overflow: hidden;
		}

		.image_block img+div {
			display: none;
		}

		@media (max-width:745px) {
			.desktop_hide table.icons-inner {
				display: inline-block !important;
			}

			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}

			.mobile_hide {
				max-width: 0;
				min-height: 0;
				max-height: 0;
				font-size: 0;
				display: none;
				overflow: hidden;
			}

			.desktop_hide,
			.desktop_hide table {
				max-height: none !important;
				display: table !important;
			}
		}
	</style>
</head>
<body style="text-size-adjust: none; background-color: #fff; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #fff;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #fcf7f7;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-top: 20px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="10" cellspacing="0" class="image_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="pad">

<div align="center" class="alignment" style="line-height:10px"><a href="https://the-favis-p5cw.vercel.app/" style="outline:none" tabindex="-1" target="_blank"><img alt="Your Logo" src="https://the-favis-p5cw.vercel.app/main-logo.png" style="height: auto; display: block; border: 0; max-width: 174px; width: 100%;" title="Your Logo" width="174"/></a></div>

</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #fcf7f7; background-size: auto;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; background-size: auto; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 15px; padding-left: 50px; padding-right: 50px; padding-top: 15px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#506bec;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:38px;line-height:120%;text-align:left;mso-line-height-alt:45.6px;">
<p style="margin: 0; word-break: break-word;"><strong><span>Forgot your password?</span></strong></p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">Dear ${firstName} ${lastName}.</p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;"><span>Hey, we received a request to reset your password.</span></p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">It appears that you've forgotten your password, but worry not! We have you covered</p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-5" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">A request to reset your password has been received and processed successfully.</p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-6" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">Below, you'll find your temporary password, which will grant you access to your account. Remember to change it after logging in for added security.</p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="button_block block-7" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="pad" style="padding-bottom:20px;padding-left:10px;padding-right:10px;padding-top:20px;text-align:left;">
<div align="left" class="alignment"><!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:46px;width:273px;v-text-anchor:middle;" arcsize="35%" stroke="false" fillcolor="#506bec"><w:anchorlock/><v:textbox inset="5px,0px,0px,0px"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:15px"><![endif]--><a href="https://the-favis-p5cw.vercel.app/login" style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#506bec;border-radius:16px;width:auto;border-top:0px solid TRANSPARENT;font-weight:undefined;border-right:0px solid TRANSPARENT;border-bottom:0px solid TRANSPARENT;border-left:0px solid TRANSPARENT;padding-top:8px;padding-bottom:8px;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;text-align:center;mso-border-alt:none;word-break:keep-all;" target="_blank"><span style="padding-left:25px;padding-right:20px;font-size:15px;display:inline-block;letter-spacing:normal;"><span style="word-break:break-word;"><span data-mce-style="" style="line-height: 30px;"><strong>Temporary Password: ${tempPass}</strong></span></span></span></a><!--[if mso]></center></v:textbox></v:roundrect><![endif]--></div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-8" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">Need any help or facing any challenges during this process? Reach out to our friendly support team at <a href="mailto:support@thefavis.com" rel="noopener" style="text-decoration: underline; color: #40507a;" target="_blank">support@thefavis.com</a>, and they will gladly assist you.</p>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="paragraph_block block-9" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="color:#40507a;font-family:Helvetica Neue, Helvetica, Arial, sans-serif;font-size:16px;line-height:120%;text-align:left;mso-line-height-alt:19.2px;">
<p style="margin: 0; word-break: break-word;">Thank you!</p>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #fcf7f7;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 5px; padding-top: 5px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="10" cellspacing="0" class="text_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad">
<div style="font-family: sans-serif">
<div class="" style="font-size: 12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #555555; line-height: 1.2;">
<p style="margin: 0; text-align: center; font-size: 16px; mso-line-height-alt: 14.399999999999999px;"> </p>
<p style="margin: 0; text-align: center; font-size: 16px; mso-line-height-alt: 19.2px;"><span style="font-size:16px;"><em><strong>We Located at 2315 Greens Road, Houston TX, 77032</strong></em></span></p>
<p style="margin: 0; text-align: center; font-size: 16px; mso-line-height-alt: 19.2px;"><span style="font-size:16px;"><em><strong>Visit us website @ <a href="https://thefavis.com/" rel="noopener" style="text-decoration: underline;" target="_blank">The Favis</a></strong></em></span></p>
<p style="margin: 0; text-align: center; font-size: 16px; mso-line-height-alt: 14.399999999999999px;"> </p>
</div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="empty_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="pad">
<div></div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-5" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 20px; padding-left: 10px; padding-right: 10px; padding-top: 10px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="empty_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="pad">
<div></div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-6" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; width: 725px; margin: 0 auto;" width="725">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 5px; padding-top: 5px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="icons_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="pad" style="vertical-align: middle; color: #9d9d9d; font-family: inherit; font-size: 15px; padding-bottom: 5px; padding-top: 5px; text-align: center;">
<table cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td class="alignment" style="vertical-align: middle; text-align: center;"><!--[if vml]><table align="left" cellpadding="0" cellspacing="0" role="presentation" style="display:inline-block;padding-left:0px;padding-right:0px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"><![endif]-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table><!-- End -->
</body>
</html>`;


		const mailOptions = {
    		 from: 'The Favis <info@thefavis.com>',
     		to: email,
     		subject:subject,
    		html: html,
		};

	return	transporter.sendMail(mailOptions).then((info,error)=>{
		
				if(error){
					return false;
				}else{
					sendNotification(reqBody);
					return true;
				}
		})

}

function sendRegistration(reqBody,userId){
	


	const {email,firstName,lastName,password}=reqBody;
	const subject="The Favis Email Verification";
	const html=`<!DOCTYPE html>

<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/><!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]--><!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet" type="text/css"/><!--<![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		body {
			margin: 0;
			padding: 0;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		.desktop_hide,
		.desktop_hide table {
			mso-hide: all;
			display: none;
			max-height: 0px;
			overflow: hidden;
		}

		.image_block img+div {
			display: none;
		}

		@media (max-width:700px) {
			.desktop_hide table.icons-inner {
				display: inline-block !important;
			}

			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}

			.mobile_hide {
				max-width: 0;
				min-height: 0;
				max-height: 0;
				font-size: 0;
				display: none;
				overflow: hidden;
			}

			.desktop_hide,
			.desktop_hide table {
				max-height: none !important;
				display: table !important;
			}
		}
	</style>
</head>
<body style="text-size-adjust: none; background-color: #fff; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #fff;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; background-color: #fcf7f7; width: 680px; margin: 0 auto;" width="680">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<div class="spacer_block block-1" style="height:5px;line-height:5px;font-size:1px;"> </div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>

</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-position: top center;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; background-color: #fcf7f7; width: 680px; margin: 0 auto;" width="680">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 20px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad" style="padding-left:10px;padding-right:10px;padding-top:10px;">
<div style="font-family: Arial, sans-serif">
<div class="" style="font-size: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #30004d; line-height: 1.2;">
<p style="margin: 0; text-align: center; mso-line-height-alt: 14.399999999999999px;"><strong><span style="font-size:16px;">The Favis Car Rental Vehicle Management Services</span></strong></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block block-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad" style="padding-left:10px;padding-right:10px;">
<div style="font-family: Arial, sans-serif">
<div class="" style="font-size: 12px; font-family: 'Abril Fatface', Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #6277fe; line-height: 1.2;">
<p style="margin: 0; text-align: center; mso-line-height-alt: 14.399999999999999px; letter-spacing: 2px;"><span style="font-size:58px;"><strong><span style=""><span style="">Welcome</span></span></strong></span></p>
</div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-4" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000; background-color: #fcf7f7; width: 680px; margin: 0 auto;" width="680">
<tbody>
<tr>
<td class="column column-1" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; text-align: left; font-weight: 400; padding-bottom: 20px; vertical-align: top; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="text_block block-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td class="pad" style="padding-bottom:10px;padding-left:20px;padding-right:20px;padding-top:10px;">
<div style="font-family: Georgia, 'Times New Roman', serif">
<div class="" style="font-size: 12px; font-family: Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 18px; color: #30004d; line-height: 1.5;">
<p style="margin: 0; text-align: left; font-size: 14px; mso-line-height-alt: 21px;"><span style="font-size:14px;">Dear ${firstName} ${lastName},</span></p>
<p style="margin: 0; text-align: left; font-size: 14px; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">We are delighted to welcome you to The Favis! Thank you for successfully registering with us. Your commitment to joining our platform is greatly appreciated.</span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Here are your login credentials: </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Username:${email} </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Password:${password}</span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">To ensure the security of your account and complete the registration process, we kindly request you to click on the verification link provided below: </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;"><a href="https://the-favis-p5cw.vercel.app/verify/${userId}">[Verification Link] </a></span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Once your email is verified, you will have full access to our platform and be able to proceed to enter your vehicle details. If you encounter any issues during the verification process or need assistance with anything else, please don't hesitate to contact our friendly support team at contactus@thefavis.com. </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 18px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Thank you again for choosing The Favis. We look forward to providing you with a wonderful experience within our community. </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Best regards, </span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">The Favis Team</span></p>
<p style="margin: 0; font-size: 14px; text-align: left; mso-line-height-alt: 21px;"><span style="font-size:14px;">Call Us(281-825-1514)</span></p>
</div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>



</td>
</tr>
</tbody>
</table><!-- End -->
</body>
</html>`;
	const mailOptions = {
    		 from: 'The Favis <info@thefavis.com>',
     		to: email,
     		subject:subject,
    		html: html,
		};
		
	


	return	transporter.sendMail(mailOptions).then((error,info)=>{
				console.log(error)
				if(error){
					return false;
				}else{
					/*sendNotification(reqBody);*/
					return true;
				}
		})



}





module.exports.topUp=async(reqBody,userId)=>{
	let userToCredit=await User.findById(reqBody.userId).then(result=>result);
		
	let newAmount=userToCredit.credit+parseInt(reqBody.amount);
	
	if(userToCredit.length===0){
		return {"success":false,"msg":"User Not Found"}
	}else{
	
		return User.updateOne({"_id":reqBody.userId} , {"$set":{"credit": newAmount}}).then((qrtoupdate,error)=>{
		if(error){
			return false;
		}else{	
			addTransaction(reqBody,userId,"Top Up");
			return {"success":true,"msg":"Amount Credited"}
		}
		})
	}

}
module.exports.checkCredit =async (userId) =>{
	
	return await User.findById(userId).then(result =>{
		return result;
	})
}


function addTransaction(reqBody,userId,type){
	let newTransaction= new TransactionHistory({
		userId:reqBody.userId,
		refId:reqBody.refId,
		type:type,
		description:reqBody.description,
		amount:reqBody.amount,
		transactBy:userId	
	})

	return 	newTransaction.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}



module.exports.creditDeduct=async(reqBody,userId)=>{
	let userInfo=await User.find({_id:userId}).then(result=>result);			
	if(userInfo[0].credit>=parseInt(reqBody.amount)){
		let newAmount=userInfo[0].credit-parseInt(reqBody.amount);
		if(userInfo[0].length===0){
			return {"success":false,"msg":"Qr Not Found"}
		}else{
			return User.updateOne({"_id":userId} , {"$set":{"credit": newAmount}}).then((qrtoupdate,error)=>{			
			if(error){
				return {"success":false,"msg":"Amount Not Debited"}
			}else{	
				if(reqBody.services=="Park & Prosper"){
					addParking(reqBody,userId);	
				}			
				updateCar(reqBody,userId);
				addTransaction(reqBody,userId);	
				return {"success":true,"msg":"Amount Debited"}
			}
			})
		}
	}else{
		return {"success":false,"msg":"Not Enough Credit"}
	}	
}
function addParking(reqBody,userId){

	let newParking= new Parking({
		refId:reqBody.vehicleId,
		plateNumber:reqBody.plateNumber,
		userId:userId,
		status:"Subscription",
		amountPaid:reqBody.amount
	})
	return 	newParking.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}
function updateCar(reqBody,userId){
  return  Car.findByIdAndUpdate(reqBody.vehicleId, {"$set":{"status":"For Approval","subscriptionType":reqBody.subscription}}).then((car,error)=>{

    if(error){      
      return false;
    }else{  
      return true;
    }
  });
}
function addTransaction(reqBody,userId){
	let newTransaction= new TransactionHistory({
		userId:reqBody.userId,
		refId:reqBody.refId,
		type:"Payment",
		description:"Client Subscription Payment",
		amount:reqBody.amount,
		transactBy:userId	
	})

	return 	newTransaction.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}


function sendNotification(reqBody){
			const {firstName,lastName,phone,address,applicationRef,email}=reqBody;
			const subject="New Inquiries";
			const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">New Application<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#fff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:37.5em">
      <tr style="width:100%">
        <td>
          <table style="padding:30px 20px" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td><center><img src="https://the-favis-p5cw.vercel.app/main-logo.png" style="display:block;outline:none;border:none;text-decoration:none" /></center></td>
              </tr>
            </tbody>
          </table>
          <table style="border:1px solid rgb(0,0,0, 0.1);border-radius:3px;overflow:hidden" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td>
                  <table width="100%" style="padding:20px 40px;padding-bottom:0" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                    <tbody style="width:100%">
                      <tr style="width:100%">
                        <td>
                          <h1 style="font-size:32px;font-weight:bold;text-align:center">Hello Admin,</h1>
                          <h2 style="font-size:26px;font-weight:bold;text-align:center">We have new application Ref:${applicationRef}</h2>
                          <p style="font-size:16px;line-height:24px;margin:16px 0"><b>First Name: </b>${firstName}  <b>Last Name:</b>${lastName}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0"><b>Phone: </b>${phone}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0;margin-top:-5px"><b>Address: </b>${address}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0;margin-top:-5px"><b>Email:</b>${email}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0">
                          <a href="https://the-favis-p5cw.vercel.app/login">Go to the admin portal</a>
                         </p>
                         
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" style="padding:20px 40px;padding-top:0" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                    <tbody style="width:100%">
                      <tr style="width:100%">
                        <td colSpan="2" style="display:flex;justify-content:center;width:100%"><a target="_blank" style="background-color:#e00707;padding:0px 0px;border-radius:3px;color:#FFF;font-weight:bold;border:1px solid rgb(0,0,0, 0.1);cursor:pointer;line-height:100%;text-decoration:none;display:inline-block;max-width:100%"><span><!--[if mso]><i style="letter-spacing: undefinedpx;mso-font-width:-100%;mso-text-raise:0" hidden>&nbsp;</i><![endif]--></span><span><!--[if mso]><i style="letter-spacing: undefinedpx;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table style="padding:45px 0 0 0" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td><img src="https://upcdn.io/12a1yVA/raw/For%20Website/multiple-car.png" width="620" style="display:block;outline:none;border:none;text-decoration:none" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </table>
  </body>

</html>`;
			const mailNotify = {
    		from: 'The Favis <info@thefavis.com>',
     		to: 'info@thefavis.com',
     		subject:subject,
    		html: html,
};

  return  transporter.sendMail(mailNotify).then((info,error)=>{
        if(error){
          return false;
        }else{
       
          return true;
        }
    })

}

function sendNotificationInquiry(reqBody){
      const {firstname,lastname,phone,address,applicationRef,email,message}=reqBody;
      const subject="New Inquiries";
      const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">New Inquiries<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#fff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:37.5em">
      <tr style="width:100%">
        <td>
          <table style="padding:30px 20px" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td><center><img src="https://the-favis-p5cw.vercel.app/main-logo.png" style="display:block;outline:none;border:none;text-decoration:none" /></center></td>
              </tr>
            </tbody>
          </table>
          <table style="border:1px solid rgb(0,0,0, 0.1);border-radius:3px;overflow:hidden" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td>
                  <table width="100%" style="padding:20px 40px;padding-bottom:0" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                    <tbody style="width:100%">
                      <tr style="width:100%">
                        <td>
                          <h1 style="font-size:32px;font-weight:bold;text-align:center">Hello Admin,</h1>
                          <h2 style="font-size:26px;font-weight:bold;text-align:center">We have new Inquiry</h2>
                          <p style="font-size:16px;line-height:24px;margin:16px 0"><b>First Name: </b>${firstname}  <b>Last Name:</b>${lastname}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0"><b>Phone: </b>${phone}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0;margin-top:-5px"><b>Address: </b>${address}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0;margin-top:-5px"><b>Email:</b>${email}</p>
                          <p style="font-size:16px;line-height:24px;margin:16px 0">
                             Message:${message}
                         </p>
                         
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" style="padding:20px 40px;padding-top:0" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                    <tbody style="width:100%">
                      <tr style="width:100%">
                        <td colSpan="2" style="display:flex;justify-content:center;width:100%"><a target="_blank" style="background-color:#e00707;padding:0px 0px;border-radius:3px;color:#FFF;font-weight:bold;border:1px solid rgb(0,0,0, 0.1);cursor:pointer;line-height:100%;text-decoration:none;display:inline-block;max-width:100%"><span><!--[if mso]><i style="letter-spacing: undefinedpx;mso-font-width:-100%;mso-text-raise:0" hidden>&nbsp;</i><![endif]--></span><span><!--[if mso]><i style="letter-spacing: undefinedpx;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table style="padding:45px 0 0 0" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            <tbody>
              <tr>
                <td><img src="https://upcdn.io/12a1yVA/raw/For%20Website/multiple-car.png" width="620" style="display:block;outline:none;border:none;text-decoration:none" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </table>
  </body>

</html>`;
      const mailNotify = {
        from: 'The Favis <info@thefavis.com>',
        to: 'info@thefavis.com',
        subject:subject,
        html: html,
};

  return  transporter.sendMail(mailNotify).then((info,error)=>{
        if(error){
          return false;
        }else{
       
          return true;
        }
    })

}

function sendNewApplication(reqBody){
   const {email,firstName,lastName,phone}=reqBody;
  const subject="The Favis";
  const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">Application Confirm Welcome to The Favis Car Rental Vehicle Rental Management Services! we will get back to you as soon as possible.<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#ffffff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:40.5em;margin:0 auto;padding:20px 0 48px">
      <tr style="width:100%">
        <td><img alt="TheFavis" src="https://the-favis-p5cw.vercel.app/main-logo.png"  style="display:block;outline:none;border:none;text-decoration:none;margin:0 auto" />
          <p style="font-size:16px;line-height:26px;margin:16px 0">Dear ${firstName} ${lastName},</p>
          <p style="font-size:16px;line-height:26px;margin:0px 0">
We are thrilled to have received your application. Thank you for considering The Favis as your potential destination. Your application has been successfully submitted, and our dedicated team will now review it. We will be in touch via both phone and email within 3 business days to provide updates on the status of your application. Please keep your communication lines open, as we may contact you via phone, and remember to check your email regularly.
<br/><br/>
Additionally, you have the flexibility to make adjustments to your application by removing any cars until it is approved. Please note that once the application has been approved, no further changes can be made. Thank you for your interest and cooperation. We look forward to the possibility of welcoming you to The Favis. 


          <table style="text-align:center" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            
          </table>
          <p style="font-size:16px;line-height:26px;margin:16px 0">Warm regards, <br />The Favis Sales team<br/> (281-825-1514/sales@thefavis.com)
          </p>
           <p style="font-size:12px;line-height:24px;margin:16px 0;color:#8898aa">2315 Greens Road, Houston TX, 77032</p>
         
          <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />
                  <table align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <table width="100%" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                            <tbody style="width:100%">
                              <tr style="width:100%">
                                <a href="https://thefavis.com/" target="_blank" style="background-color:#656ee8;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;line-height:100%;max-width:100%;padding:10px 10px"><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%;mso-text-raise:15" hidden>&nbsp;</i><![endif]--></span><span style="background-color:#656ee8;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;max-width:100%;line-height:120%;text-transform:none;mso-padding-alt:0px;mso-text-raise:7.5px">Go to our Website</span><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a>
                                <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />

         
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>

        </td>
      </tr>
    </table>

  </body>

</html>`;

 const mailOptions = {
         from: 'The Favis <info@thefavis.com>',
        to: email,
        subject:subject,
        html: html,
    };

  return  transporter.sendMail(mailOptions).then((info,error)=>{

        if(error){
          return false;
        }else{
          sendNotification(reqBody);
          return true;
        }
    })



}
module.exports.sendNewInquiry=(reqBody)=>{

     

  const {email,firstname,lastname,phone}=reqBody;
  const subject="The Favis";
  const html=`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<html lang="en">

  <head></head>
  <div id="__react-email-preview" style="display:none;overflow:hidden;line-height:1px;opacity:0;max-height:0;max-width:0">Inquiries Confirm Welcome to The Favis Car Rental Vehicle Rental Management Services! we will get back to you as soon as possible.<div> ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿ ‌​‍‎‏﻿</div>
  </div>

  <body style="background-color:#ffffff;font-family:-apple-system,BlinkMacSystemFont,&quot;Segoe UI&quot;,Roboto,Oxygen-Sans,Ubuntu,Cantarell,&quot;Helvetica Neue&quot;,sans-serif">
    <table align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0" width="100%" style="max-width:40.5em;margin:0 auto;padding:20px 0 48px">
      <tr style="width:100%">
        <td><img alt="TheFavis" src="https://the-favis-p5cw.vercel.app/main-logo.png"  style="display:block;outline:none;border:none;text-decoration:none;margin:0 auto" />
          <p style="font-size:16px;line-height:26px;margin:16px 0">Hello ${firstname} ${lastname},</p>
          <p style="font-size:16px;line-height:26px;margin:0px 0">

We're thrilled about your inquiries to Favis. A dedicated representative will contact you within 24 hours. 
To ensure smooth communication, please confirm your accurate mobile number: (${phone}). 
If this number is incorrect or needs to be updated, please don't hesitate to respond to this email with the correct contact number.

<br/><br/>Thanks for your patience.


          <table style="text-align:center" align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
            
          </table>
          <p style="font-size:16px;line-height:26px;margin:16px 0">Best Regards,<br />Sales Department The Favis<br/> (281-825-1514/sales@thefavis.com)
          </p>
           <p style="font-size:12px;line-height:24px;margin:16px 0;color:#8898aa">2315 Greens Road, Houston TX, 77032</p>
         
          <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />
                  <table align="center" border="0" cellPadding="0" cellSpacing="0" role="presentation" width="100%">
                    <tbody>
                      <tr>
                        <td>
                          <table width="100%" align="center" role="presentation" cellSpacing="0" cellPadding="0" border="0">
                            <tbody style="width:100%">
                              <tr style="width:100%">
                                <a href="https://thefavis.com/" target="_blank" style="background-color:#656ee8;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;line-height:100%;max-width:100%;padding:10px 10px"><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%;mso-text-raise:15" hidden>&nbsp;</i><![endif]--></span><span style="background-color:#656ee8;border-radius:5px;color:#fff;font-size:16px;font-weight:bold;text-decoration:none;text-align:center;display:inline-block;width:100%;p-x:10px;p-y:10px;max-width:100%;line-height:120%;text-transform:none;mso-padding-alt:0px;mso-text-raise:7.5px">Go to our Website</span><span><!--[if mso]><i style="letter-spacing: 10px;mso-font-width:-100%" hidden>&nbsp;</i><![endif]--></span></a>
                                <hr style="width:100%;border:none;border-top:1px solid #eaeaea;border-color:#cccccc;margin:20px 0" />

         
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>

        </td>
      </tr>
    </table>

  </body>

</html>`;
  const mailOptions = {
         from: 'The Favis <info@thefavis.com>',
        to: email,
        subject:subject,
        html: html,
    };

  return  transporter.sendMail(mailOptions).then((info,error)=>{
        if(error){
          return false;
        }else{
          sendNotificationInquiry(reqBody);
          return true;
        }
    })

	}
