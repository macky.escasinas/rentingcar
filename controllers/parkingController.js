const auth=require("../auth");
const Parking=require("../models/Parking");
const TransactionHistory=require("../models/TransactionHistory");

module.exports.addParking=(reqBody)=>{

	let newParking= new Parking({
		refId:reqBody.qr,
		plateNumber:reqBody.plateNumber
		
	})

	return 	newParking.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});

	
}

module.exports.viewQr=(reqBody)=>{
	return Parking.find({refId:reqBody.qrcode,isActive:true}).then((parkingDetails,error)=>{

		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}
module.exports.entryQr=(reqBody)=>{
	return Parking.findOneAndUpdate({refId:reqBody.qrcode,isActive:true}, {"$set":{"status": "Not Paid","amountPaid":0,"dateStarted":new Date()}}).then((parkingDetails,error)=>{
			
		if(error){			
			return {"success":false,"msg":parkingDetails}

		}else{	
			return {"success":true,"msg":parkingDetails}
			
		}
	});
}


module.exports.cashPayment=async(reqBody,userId)=>{
	
	let parkingUpdate= await Parking.updateOne({"refId": reqBody.refId} , {"$set":{"status": "Completed","amountPaid": reqBody.amount}}).then((result,error)=>{
		if(error){
			return false;
		}else{
			addTransaction(reqBody,userId);
		}
	});

	return true;
	
}

module.exports.OnlinePayment=async(reqBody,userId)=>{
	
	let parkingUpdate= await Parking.updateOne({"refId": reqBody.refId} , {"$set":{"status": "Paid","amountPaid": reqBody.amount}}).then((result,error)=>{
		if(error){
			return false;
		}else{
			addTransaction(reqBody,userId);
		}
	});

	return true;
	
}
module.exports.topUp=async(reqBody,userId)=>{
	let qrToCredit=await Parking.find({refId:reqBody.userId,isActive:true}).then(result=>result);
	let newAmount=qrToCredit[0].creditAmount+parseInt(reqBody.amount);
	if(qrToCredit.length===0){
		return {"success":false,"msg":"Qr Not Found"}
	}else{
	
		return Parking.updateOne({"refId":reqBody.userId} , {"$set":{"creditAmount": newAmount}}).then((qrtoupdate,error)=>{
		if(error){
			return false;
		}else{	
			return {"success":true,"msg":"Amount Credited"}
			addTransaction(reqBody,userId);
			
		}
		})
	}

}

module.exports.creditPayment=async(reqBody,userId)=>{
	
	let qrToCredit=await Parking.find({refId:reqBody.refId,isActive:true}).then(result=>result);
	
	

	if(qrToCredit[0].creditAmount>=parseInt(reqBody.amount)){
		let newAmount=qrToCredit[0].creditAmount-parseInt(reqBody.amount);
		if(qrToCredit.length===0){
			return {"success":false,"msg":"Qr Not Found"}
		}else{

			return Parking.updateOne({"refId":reqBody.userId,"status":{"$ne":"Completed"}} , {"$set":{"creditAmount": newAmount,"status": "Completed"}}).then((qrtoupdate,error)=>{
			
			if(error){
				return {"success":false,"msg":"Amount Not Debited"}
			}else{	
				
				return {"success":true,"msg":"Amount Debited"}
				addTransaction(reqBody,userId);
			}
			})
		}
	}else{
		return {"success":false,"msg":"Not Enough Credit"}
	}

	

}

function addTransaction(reqBody,userId){
	let newTransaction= new TransactionHistory({
		userId:reqBody.userId,
		refId:reqBody.refId,
		type:"Parking",
		description:reqBody.description,
		amount:reqBody.amount,
		transactBy:userId	
	})

	return 	newTransaction.save().then((parkingDetails,error)=>{
		if(error){			
			return false;
		}else{	
			return parkingDetails;
		}
	});
}