		const User=require("../models/User");
const bcrypt=require("bcrypt");
const auth=require("../auth");
const Parameter=require("../models/Parameter");


module.exports.addNewParameter= (reqBody)=>{
	let newParameter= new Parameter({
		type: reqBody.type,
		name: reqBody.name,
	})	

	return 	newParameter.save().then((car,error)=>{
		if(error){			
			return false;
		}else{	
			return true;
		}
	});

}
module.exports.viewType=()=>{
	return Parameter.find({type:"condition",isActive:true}).then(result=>result);
}
module.exports.viewAmount=(reqBody)=>{

	return Parameter.find({type:"Services",name:reqBody.services,subtype:reqBody.range}).then(result=>result);
}

