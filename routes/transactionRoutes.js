const express=require("express");
const router=express.Router();
const transactionHistoryController=require("../controllers/transactionHistoryController");
const auth=require("../auth");


router.get("/all-transaction", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin==true){
		transactionHistoryController.viewAllTransaction(req.body).then(transactionHistoryController=>res.send(transactionHistoryController));
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
})
router.get("/client-transaction", auth.verify, (req, res) =>{

		const userData=auth.decode(req.headers.authorization);

		transactionHistoryController.viewClientTransaction(userData.id).then(transactionHistoryController=>res.send(transactionHistoryController));

	
})
module.exports = router;