const express=require("express");
const router=express.Router();
const apiController=require("../controllers/apiController");
const auth=require("../auth");


router.post("/generate", auth.verify,(req,res)=>{
		const userData=auth.decode(req.headers.authorization);
		if(userData.isAdmin==true){
			apiController.generateApi().then(apiController=>res.send(apiController));			
		}else{
			res.send(false);
		}
			
})
router.post("/generate-qr", auth.verify,(req,res)=>{
		const userData=auth.decode(req.headers.authorization);

		if(userData.isAdmin==true){
			apiController.generateQr(req.body.plateNumber).then(apiController=>res.send(apiController));			
		}else{
			res.send(false);
		}
			
})


module.exports = router;


