const express=require("express");
const router=express.Router();
const parkingController=require("../controllers/parkingController");
const auth=require("../auth");
const apiController=require("../controllers/apiController");

router.post("/add-qr", auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization);
		if(userData.isAdmin==true){
			parkingController.addParking(req.body).then(parkingController=>res.send(parkingController));		
		}else{
			res.send(false);
		}
		
			
})
router.post("/view-qr", (req,res)=>{

		parkingController.viewQr(req.body).then(parkingController=>res.send(parkingController));
		
			
})
router.post("/reentry-qr", (req,res)=>{

		parkingController.entryQr(req.body).then(parkingController=>res.send(parkingController));
		
			
})
router.post("/cash-payment", auth.verify, (req,res)=>{
		const userData=auth.decode(req.headers.authorization);
		if(userData.isAdmin==true){
			parkingController.cashPayment(req.body,userData.id).then(parkingController=>res.send(parkingController));
		}else{
			res.send(false);
		}			
})
router.post("/online-payment",async (req,res)=>{
		if(req.headers.authorization!=undefined){
			const apikey = req.headers.authorization.split(' ')[1]
						
			const checkApi=await apiController.checkApi(apikey).then(apiController=>
				{
					if(apiController==true){
							return apiController
					}else{
						return false;
					}
				}
				
			);

			if(checkApi){
				
				parkingController.OnlinePayment(req.body,req.body.userId).then(parkingController=>
				{

					if(parkingController==true){
							res.status(201);
							res.send({"success":parkingController,"msg":"Successfully Paid"});
					}else{

						res.status(200);
						res.send({"success":false,"msg":parkingController});
					}

				}

				);
			}else{
				res.status(403);
				res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
			}

		}else{
			res.status(401);
			res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
		}
		
		
					
})
router.get("/all-transaction", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.isAdmin==true){
		parkingController.viewAllTransaction(req.body).then(applicationController=>res.send(applicationController));
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
})
router.post("/credit-deduct", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);

	
	
	if(userData.isAdmin==true){
		parkingController.creditPayment(req.body,userData.id).then(applicationController=>res.send(applicationController));
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
})
router.post("/qr-topup", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);

	
	
	if(userData.isAdmin==true){
		parkingController.topUp(req.body,userData.id).then(applicationController=>res.send(applicationController));
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
})
router.post("/qr-topAccount",async (req,res)=>{
		if(req.headers.authorization!=undefined){
			const apikey = req.headers.authorization.split(' ')[1]
						
			const checkApi=await apiController.checkApi(apikey).then(apiController=>
				{
					if(apiController==true){
							return apiController
					}else{
						return false;
					}
				}
				
			);

			if(checkApi){
				parkingController.topUp(req.body,req.body.userId).then(parkingController=>
				{

					if(parkingController==true){
							res.status(201);
							res.send({"success":parkingController,"msg":"Successfully Paid"});
					}else{

						res.status(200);
						res.send({"success":false,"msg":parkingController});
					}

				}

				);
			}else{
				res.status(403);
				res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
			}

		}else{
			res.status(401);
			res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
		}
		
		
					
})



module.exports = router;


