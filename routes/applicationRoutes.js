const express=require("express");
const router=express.Router();
const applicationController=require("../controllers/applicationController");
const auth=require("../auth");

router.post("/notify-user", (req, res) =>{
	
	applicationController.sendNewInquiry(req.body).then(applicationController=>res.send(applicationController));
})

router.post("/create-application", auth.verify,(req,res)=>{
			const userData=auth.decode(req.headers.authorization);
			applicationController.createApplication(userData.id).then(applicationController=>res.send(applicationController));
})

router.post("/check-application", auth.verify,(req,res)=>{
			const userData=auth.decode(req.headers.authorization);
			applicationController.checkApplication(userData.id).then(applicationController=>res.send(applicationController));
})

router.get("/all-application", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.userType=="admin"){
		applicationController.viewAllApplication(req.body).then(applicationController=>res.send(applicationController));
	}else{
			applicationController.checkApplication(userData.id).then(applicationController=>res.send(applicationController));
	}
	
})
router.post("/today-application", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.userType=="admin"){
		applicationController.todayAllApplication(req.body).then(applicationController=>res.send(applicationController));
	}else{
			applicationController.checkApplication(userData.id).then(applicationController=>res.send(applicationController));
	}
	
})
router.post("/search-application", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.userType=="admin"){
		applicationController.searchAllApplication(req.body).then(applicationController=>res.send(applicationController));
	}else{
			applicationController.checkApplication(userData.id).then(applicationController=>res.send(applicationController));
	}
	
})


router.get("/:applicationId", auth.verify, (req,res)=>{
		console.log(req.params.applicationId);

	applicationController.getApplication(req.params.applicationId).then(applicationController=>
			res.send(applicationController));

})


router.post("/update-application/:applicationId", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization);

			applicationController.updateApplication(req.params.applicationId,userData.id,req.body).then(applicationController=>res.status(200).send(applicationController));
	
})

module.exports = router;


