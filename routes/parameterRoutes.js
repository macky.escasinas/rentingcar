const express=require("express");
const router=express.Router();
const parameterController=require("../controllers/parameterController");
const auth=require("../auth");

router.post("/add-new", auth.verify, (req,res)=>{
		const userData=auth.decode(req.headers.authorization);
		console.log(userData);
		if(userData.userType=="Admin"){
			parameterController.addNewParameter(req.body).then(parameterController=>res.send(parameterController));
		}else{
			res.status(404);
			res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});

		}
})
router.get("/view-type",(req,res)=>{
	parameterController.viewType(req.body).then(parameterController=>res.send(parameterController));

})

router.post("/amount-check/", auth.verify,(req,res)=>{
	parameterController.viewAmount(req.body).then(parameterController=>res.send(parameterController));
})
module.exports = router;