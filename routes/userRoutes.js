const express=require("express");
const router=express.Router();
const userControllers=require("../controllers/userController");
const apiController=require("../controllers/apiController");
const auth=require("../auth");

//Register
router.post("/register", async (req,res)=>{
	
		
		if(req.headers.authorization!=undefined){
			const apikey = req.headers.authorization.split(' ')[1]
				console.log(apikey);
			const checkApi=await apiController.checkApi(apikey).then(apiController=>
				{
					if(apiController==true){
							return apiController
					}else{
						return false;
					}
				}
				
			);

			if(checkApi){
				userControllers.registerUser(req.body).then(userController=>
				{

					if(userController==true){
							res.status(201);
							res.send({"success":userController,"msg":"Successfully Added"});
					}else{

						res.status(200);
						res.send({"success":false,"msg":userController});
					}

				}

				);
			}else{
				res.status(403);
				res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
			}

		}else{
			res.status(401);
			res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
		}
	
})
router.post("/verify",(req,res)=>{
	
	userControllers.verifyUser(req.body).then(userController=>res.send(userController));
})
//Login
router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(userController=>res.send(userController));
})

//Get Profile
router.get("/profile", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization); 

	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})


router.post("/applicationProfile", auth.verify, (req, res) =>{
	
	const userData = auth.decode(req.headers.authorizationtion); 

	userControllers.getFullProfile(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/full-profile", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); 
	userControllers.getFullProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})
//Get All Car Owner
router.get("/car-owner", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	if(userData.userType=="admin"){
		userControllers.viewAllCarOwner(req.body).then(userController=>res.send({"data":userController}));
	}else{
		res.send(false);
	}
	
})

router.post("/checkEmail", (req, res) =>{

	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/reset-password", (req, res) =>{
	
	userControllers.resetPassword(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/change-password", auth.verify, (req, res) =>{
	
	const userData=auth.decode(req.headers.authorization);

	userControllers.changePassword(req.body,userData).then(resultFromController => res.send(resultFromController));
});


router.post("/login",(req,res)=>{

	userControllers.loginUser(req.body).then(userController=>res.send(userController));
})
router.post("/add-credit",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	
	
	if(userData.isAdmin){
		userControllers.addCredit(req.body,userData.id).then(userController=>res.send(userController));
		
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
	
})
router.post("/top-up",auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		userControllers.addCredit(req.body,userData.id).then(userController=>res.send(userController));
		
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	
	}
	
	
})



router.get("/viewall-user",auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userControllers.viewAllUser(req.body,userData.id).then(userController=>res.send(userController));
	}else{
		res.status(404);
		res.send({"success":false,"msg":"You do not have permission to access, please contact the administrator"});
	}
})
router.post("/user-topAccount", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);

	userControllers.topUp(req.body,userData.id).then(applicationController=>res.send(applicationController));
	
	
})
router.post("/user-credit", auth.verify, (req, res) =>{

	const userData=auth.decode(req.headers.authorization);
	

	userControllers.checkCredit(userData.id).then(applicationController=>res.send(applicationController));
	
	
})

router.post("/credit-deduct", auth.verify, (req, res) =>{
	
	const userData=auth.decode(req.headers.authorization);
	
	userControllers.creditDeduct(req.body,userData.id).then(applicationController=>res.send(applicationController));

	
})



module.exports = router;