const express=require("express");
const router=express.Router();
const carControllers=require("../controllers/carController");
const auth=require("../auth");

router.get("/viewall-active-car",(req,res)=>{

			carControllers.viewAllActiveCar().then(carController=>res.send(carController));			
	
})
router.get("/viewall-host-car",(req,res)=>{
	
			 carControllers.viewAllHostCar().then(carController=>res.send(carController));
})


router.get("/viewall-application-car/:applicationid",(req,res)=>{
	
			 carControllers.viewAllByApplication(req.params.applicationid).then(carController=>res.send(carController));
})
router.get("/view-co-host-car",auth.verify,(req,res)=>{	
	const userData=auth.decode(req.headers.authorization);
		if(userData.userType=="admin"){
			carControllers.viewAllUserCar().then(carController=>res.send(carController));			
		}else{
			carControllers.viewAllCoHostCar(userData.id).then(carController=>res.send(carController));
		}
	
})
router.get("/view-no-application",auth.verify,(req,res)=>{	
	const userData=auth.decode(req.headers.authorization);
		if(userData.userType=="admin"){
			carControllers.viewAllUserCar().then(carController=>res.send(carController));			
		}else{
			carControllers.viewAllNoApplication(userData.id).then(carController=>res.send(carController));
		}
	
})

router.post("/delete/:vehicleId",auth.verify,(req,res)=>{
				const userData=auth.decode(req.headers.authorization);
			carControllers.deleteCar(req.params.vehicleId,userData.id).then(carController=>res.send(carController));
})

router.post("/create-car",auth.verify,(req,res)=>{

			carControllers.createCar(req.body).then(carController=>res.send(carController));
})

router.post("/add-car",auth.verify,(req,res)=>{	
	const userData=auth.decode(req.headers.authorization);
	carControllers.addCarToApplication(userData.id,req.body).then(orderController=>{
		if(orderController==true){
			res.status(201);
			res.send({"success":orderController,"msg":"Successfully Added"});
			
		}else{
			res.status(200);
			res.send({"success":orderController,"msg":"Unsuccessfull"});
		}
	});


})

router.post("/view-vehicle/:vehicleId",auth.verify,(req,res)=>{	
	const userData=auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		carControllers.viewAdminCurrentCar(req.params.vehicleId).then(carController=>res.send(carController));


	}else{
		carControllers.viewCurrentCar(req.params.vehicleId,userData.id).then(carController=>res.send(carController));

	}

})


router.post("/update-subscription",auth.verify,(req,res)=>{
			const userData=auth.decode(req.headers.authorization);
			carControllers.updateSubscription(req.body,userData.id).then(carController=>res.send(carController));
})
router.post("/add-additional-services",auth.verify,(req,res)=>{
			const userData=auth.decode(req.headers.authorization);
			carControllers.additionalServices(req.body,userData.id).then(carController=>res.send(carController));
})


module.exports = router;