const mongoose=require("mongoose");	

const applicationSchema = new mongoose.Schema({
	userId: {
		type:String
	}, 
	applicationRef: {
		type:String
	}, 
	firstName: {
		type:String
	}, 
	lastName: {
		type:String
	}, 
	email: {
		type:String
	},
	address: {
		type:String
	},  
	phone: {
		type:String
	}, 
	status: {
		type:String,
		default:"Processing"

	}, 
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("Application", applicationSchema);
