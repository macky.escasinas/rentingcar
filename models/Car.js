const mongoose=require("mongoose");	
const carSchema = new mongoose.Schema({
	userId:{
		type:String,
		require:[true,"user is required"]
	},
	make: {
		type:String,
		require:[true,"make is required"]
	}, 
	model: {
		type:String,
		require:[true,"model is required"]
	},
	year: {
		type:String,
		require:[true,"year is required"]
	},
	condition: {
		type:String,
		require:[true,"condition is required"]
	},
	costRepair: {
		type:String,
		require:[true,"costRepair is required"]
	},
	marketValue: {
		type:String,
		require:[true,"marketValue is required"]
	},
	type: {
		type:String,
		default:[true,"type is required"]
	},
	location: {
		type:String,
		default:""
	},
	description: {
		type:String,
		default:""
	},	
	gas: {
		type:String,
		default:""
	},
	transmission: {
		type:String,
		default:""
	},
	trips: {
		type:String,
		default:""
	},
	price: {
		type:Number,
		default:0
	},	
	amountPaid: {
		type:Number,
		default:0
	},
	status: {
		type:String,
		default:"For Processing"
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	},
	
	turoLink:{
		type:String,
		default:""
	},
	applicationRef:{
		type:String,
		default:""
	},
	services:{
		type:String,
		default:""	
	},
	subscriptionType:{
		type:String,
		default:""	
	},
	plateNumber:{
		type:String,
		default:""
		
	},
	qrCode:{
		type:String,
		default:""
		
	},
	additionalServices:{
		type:Object,
		default:""	
	},
	carImage:{
		type:String,
	},
	message:{
		type:String,
		default:""
	},
	
})
module.exports=mongoose.model("Car", carSchema);