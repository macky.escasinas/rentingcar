const mongoose=require("mongoose");	

const historySchema = new mongoose.Schema({
	userId: {
		type:String,
		require:[true,"User is required"]
	},
	refId: {
		type:String,
		require:[true,"TransactionId is required"]
	}, 
	type: {
		type:String,
		require:[true,"type is required"]	
	}, 
	description: {
		type:String,
		default:""
	},
	amount: {
		type:Number,
		default:0
	},
	status: {
		type:String,
		default:"Completed"
	},
	adminStatus: {
		type:String,
		default:"For Checking"
	},
	transactBy: {
		type:String,
		
	}, 
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("TransactionHistory", historySchema);
