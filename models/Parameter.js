const mongoose=require("mongoose");	

const parameterSchema = new mongoose.Schema({
	type: {
		type:String,
		require:[true,"type is required"]
	}, 
	name: {
		type:String,
		require:[true,"Name is required"]
	}
	,subtype: {
		type:String,
		default:""
	},
	amount: {
		type:Number,
		default:0
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("Parameter", parameterSchema);
