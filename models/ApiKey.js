const mongoose=require("mongoose");	

const apiSchema = new mongoose.Schema({
	apiId: {
		type:String
	}, 
	apiKey: {
		type:String
	}, 
	isActive: {
		type:Boolean,
		default:false

	}, 
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("ApiKey", apiSchema);
