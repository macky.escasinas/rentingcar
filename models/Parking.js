const mongoose=require("mongoose");	

const parkingSchema = new mongoose.Schema({
	userId: {
		type:String,
		default:"Temporary"
	},
	refId:{
		type:String,
		require:[true,"plateNumber is required"]
	},
	plateNumber: {
		type:String,
		require:[true,"plateNumber is required"]
	},
	dateStarted: {
		type:String,
		default:new Date()
	},
	dateExpired:{
		type:Date,
		default:""
	},
	amount: {
		type:Number,
		default:0
	},
	creditAmount: {
		type:Number,
		default:0
	},
	amountPaid: {
		type:Number,
		default:0
	},
	status: {
		type:String,
		default:"For Payment"
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createOn: {
		type:Date,
		default:new Date()
	}

})
module.exports=mongoose.model("Parking", parkingSchema);
