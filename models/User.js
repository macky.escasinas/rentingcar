const mongoose=require("mongoose");	

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		require:[true,"firstName is required"]
	}, 
	lastName: {
		type:String,
		require:[true,"lastName is required"]
	}, 
	userType: {
		type:String,
		require:[true,"type is required"]
	},
	email: {
		type:String,
		require:[true,"email is required"]
	}, 
	phone: {
		type:String,
		require:[true,"email is required"]
	},
	vehicle: {
		type:Number,
	}, 
	turoLink: {
		type:String
	}, 
	address: {
		type:String,
		default:""
	}, 
	company: {
		type:String,
		default:""
	}, 
	city: {
		type:String,
		default:""
	}, 
	state: {
		type:String,
		default:""
	}, 
	zip: {
		type:String,
		default:""
	}, 
	country: {
		type:String,
		default:""
	}, 
	password: {
		type:String,
		require:[true,"password is required"]
	},
	credit: {
		type:Number,
		default:0
	},
	isAdmin: {
		type:Boolean,
		default:false
	},
	verificationStatus: {
		type:Boolean,
		default:true

	},
	isReset: {
		type:Boolean,
		default:false
	},
	createOn: {
		type:String,
		default:new Date()
	}

})
module.exports=mongoose.model("User", userSchema);
